#include "Application/ChaloEngineProgram.hpp"

#include "States/StartupState.hpp"

int main()
{
    ChaloEngineProgram program;
    program.Run();
}
