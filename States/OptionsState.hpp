#ifndef _OPTIONSSTATE
#define _OPTIONSSTATE

#include "CursorState.hpp"

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"

#include <SFML/Audio.hpp>

#include <vector>

class OptionsState : public CursorState
{
public:
    OptionsState();

    void LoadSavedOptions();
    void IncreaseSoundVolume();
    void DecreaseSoundVolume();
    void IncreaseMusicVolume();
    void DecreaseMusicVolume();

    void ToggleOpenDyslexic();
    void ToggleSubtitles();
    void ToggleCaptions();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    private:
    sf::Sound m_soundTest;

};

#endif
