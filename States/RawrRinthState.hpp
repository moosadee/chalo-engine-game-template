#ifndef _RAWR_RINTH_STATE
#define _RAWR_RINTH_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"
#include "../chalo-engine/GameObjects/Projectile.hpp"
#include "../chalo-engine/GameObjects/Tile.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"

#include "CursorState.hpp"

#include <vector>

#include "../Packages/RawrRinth/BasicObject.h"
#include "../Packages/RawrRinth/Character.h"
#include "../Packages/RawrRinth/Enemy.h"
#include "../Packages/RawrRinth/Enums.h"
#include "../Packages/RawrRinth/Game.h"
#include "../Packages/RawrRinth/Item.h"
#include "../Packages/RawrRinth/Level.h"
#include "../Packages/RawrRinth/Rect.h"
#include "../Packages/RawrRinth/TextEffect.h"
#include "../Packages/RawrRinth/Tile.h"

namespace RawrRinth
{

class RawrRinthGameState : public CursorState
{
public:
    RawrRinthGameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    Game game;

    chalo::UILabel lblEggs;
    chalo::UILabel lblFruit;
    chalo::UILabel lblEnemies;
    chalo::UILabel lblMoney;

    int fruitCollected = 0, enemiesKilled = 0;
    int offsetX = 0, offsetY = 0, frame = 0;
    float gameTimer = 0.0;
    float soundTimer = 0, soundTimer2 = 0;
    bool drawMiniMap = true;

    sf::Sprite imgTileset;
    sf::Sprite imgPlayer;
    sf::Sprite imgPlayer2;
    sf::Sprite imgPlayer3;
    sf::Sprite imgPlayer4;
    sf::Sprite imgRobot;
    sf::Sprite imgEwok;
    sf::Sprite imgSnake;
    sf::Sprite imgKitty;
    sf::Sprite imgBackground;
    sf::Sprite imgCherry;
    sf::Sprite imgIcecream;
    sf::Sprite imgEgg;
    sf::Sprite imgMoney;
    sf::Sprite imgHudItem;

    float keyTimer = 0;

    Level level[1];

    Enemy enemy[ 20 ];
    Item item[ 30 ];

    int playerCount;
    Character player[4];

    TextEffect txteffect[20];

    const int MaxText = 20;     //Maximum amount of text on screen at once (too lazy to use dynamic array!)
    const int VIEWDIST = 250;   //distance enemies can see

    void UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game );
    volatile long counter = 0;
    void IncrementCounter();
    bool IsCollision( Rect r1, Rect r2 );
    bool IsCollision( Character *player, Level *level, int dir );
    bool IsCollision( Enemy *player, Level *level, int dir );
    bool IsCollision( Character *player, Item *item );
    bool IsCollision( Character *player, Enemy *enemy );
    float Distance( int x1, int y1, int x2, int y2 );
    void DrawMenuText( sf::RenderWindow& window );
    void TextAnimation( sf::RenderWindow& window , string text, float counter );
    void DrawMiniMap( sf::RenderWindow& window , Level *level, Character *player, Character *player2, Character *player3, Character *player4 );

};

}

#endif
