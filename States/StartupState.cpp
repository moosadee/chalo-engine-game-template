#include "StartupState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/Platform.hpp"

StartupState::StartupState()
{
}

void StartupState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "StartupState::Init", "function-trace" );
    CursorState::Init( name, true );
}

void StartupState::Setup()
{
    chalo::Logger::Out( "", "StartupState::Setup", "function-trace" );
    IState::Setup();
    CursorState::Setup();

    chalo::TextureManager::Add( "bg",                      "Content/Graphics/UI/menubg.png" );
    chalo::TextureManager::Add( "logo",                    "Content/Graphics/UI/logo-moosadee-small.png" );
    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Content/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "button-frame",            "Content/Graphics/UI/button-frame.png" );
    chalo::TextureManager::Add( "menu-icons",              "Content/Graphics/UI/menu-icons.png" );
    chalo::TextureManager::Add( "social-icons",            "Content/Graphics/UI/social-icons.png" );
    chalo::TextureManager::Add( "icon1",                   "Content/Graphics/UI/bigicon1.png" );

    chalo::MenuManager::LoadTextMenu( "startup.chalomenu" );

    chalo::InputManager::Setup();
}

void StartupState::Cleanup()
{
    chalo::Logger::Out( "", "StartupState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void StartupState::Update()
{
    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

    // Main menu
    if ( clickedButton == "btnPlay" ||
        ( gamepadButton == "btnPlay" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        chalo::MenuManager::LoadTextMenu( "demos.chalomenu" );

    }
    else if ( clickedButton == "btnOptions" ||
        ( gamepadButton == "btnOptions" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "optionsstate" );
    }
    else if ( clickedButton == "btnHelp" ||
        ( gamepadButton == "btnHelp" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "helpstate" );
    }
    else if ( clickedButton == "btnLanguage" ||
        ( gamepadButton == "btnLanguage" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "languagestate" );
    }
    else if ( clickedButton == "btnQuit" ||
        ( gamepadButton == "btnQuit" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        chalo::Application::ReadyToQuit();
    }
    // Social media
    else if ( clickedButton == "btnWebpage" )       { chalo::Platform::OpenUrl( "http://www.moosadee.com" ); }
    else if ( clickedButton == "btnItchio" )        { chalo::Platform::OpenUrl( "https://moosader.itch.io/" ); }
    else if ( clickedButton == "btnGooglePlay" )    { chalo::Platform::OpenUrl( "http://www.moosadee.com/" ); }
    else if ( clickedButton == "btnGitLab" )        { chalo::Platform::OpenUrl( "https://gitlab.com/RachelWilShaSingh" ); }
    else if ( clickedButton == "btnTwitter" )       { chalo::Platform::OpenUrl( "http://www.twitter.com/moosader" ); }
    else if ( clickedButton == "btnFacebook" )      { chalo::Platform::OpenUrl( "https://www.facebook.com/Moosadee" ); }
    else if ( clickedButton == "btnYouTube" )       { chalo::Platform::OpenUrl( "https://www.youtube.com/c/RachelMorrisMoosader" ); }
    else if ( clickedButton == "btnRedbubble" )     { chalo::Platform::OpenUrl( "https://www.redbubble.com/people/moosader/shop?artistUserName=Moosader&asc=u&iaCode=all-departments&sortOrder=top%20selling" ); }
    // Demo select menu
//    else if ( clickedButton == "btnBack" )
//    {
//        chalo::MenuManager::LoadTextMenu( "startup.chalomenu" );
//    }
    else if ( clickedButton == "btnDemo1" ||
        ( gamepadButton == "btnDemo1" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        SetGotoState( "pickinsticksstate" );
    }
    else if ( clickedButton == "btnDemo2" )
    {
    }
}

void StartupState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
    chalo::DrawManager::AddUILabel( m_helperLanguage );
    chalo::DrawManager::AddUILabel( m_targetLanguage );
    CursorState::Draw( window );
}



