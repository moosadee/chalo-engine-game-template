#ifndef _RAWR_ENUMS
#define _RAWR_ENUMS

namespace RawrRinth
{

enum Direction { LEFT = 1, RIGHT = 0, UP = 2, DOWN = 3 };
enum Action { WALKING = 0, ATTACKING = 1 };
enum Type { ROBOT = 0, EWOK = 1, KITTY = 2, SNAKE = 3 };

}

#endif
