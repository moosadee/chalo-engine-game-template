#ifndef _RAWR_TEXTEFFECT_HPP
#define _RAWR_TEXTEFFECT_HPP

#include "../../chalo-engine/UI/UILabel.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"

#include <string>
using namespace std;

namespace RawrRinth
{

const float increment = 1;
const float CounterAmt = 60;

class TextEffect
{
    public:
        string text;
        int x, y;
        float timer;
        int r, g, b;
        bool inUse;
        void Draw( sf::RenderWindow& window, int offsetX, int offsetY )
        {
            chalo::UILabel label;
            label.Setup( "texteffect", "main", 20, sf::Color( r, g, b ),
                sf::Vector2f( x - offsetX + 6, y + timer - offsetY - 64 ), text );
            window.draw( label.GetText() );
            //shadow
//            textprintf_centre_ex( buffer, font, x - offsetX + 6+1, y + timer - offsetY - 64, makecol( 0, 0, 0 ), -1, text.c_str() );
//            textprintf_centre_ex( buffer, font, x - offsetX + 6-1, y + timer - offsetY - 64, makecol( 0, 0, 0 ), -1, text.c_str() );
//            textprintf_centre_ex( buffer, font, x - offsetX + 6, y + timer - offsetY - 64+1, makecol( 0, 0, 0 ), -1, text.c_str() );
//            textprintf_centre_ex( buffer, font, x - offsetX + 6, y + timer - offsetY - 64-1, makecol( 0, 0, 0 ), -1, text.c_str() );
//            //text
//            textprintf_centre_ex( buffer, font, x - offsetX + 6, y + timer - offsetY - 64, makecol( r, g, b ), -1, text.c_str() );
        }
        void Setup( string message, int beginX, int beginY )
        {
            Setup( message, beginX, beginY, 255, 255, 255 );
        }
        void Setup( string message, int beginX, int beginY, int beginR, int beginG, int beginB )
        {
            x = beginX;
            y = beginY;
            r = beginR;
            g = beginG;
            b = beginB;
            timer = CounterAmt;
            inUse = true;
            text = message;
        }
        TextEffect()
        {
            inUse = false;
            x = y = r = g = b = 0;
            timer = 0;
        }
        void Update()
        {
            if ( inUse )
            {
                timer -= increment;
                if ( timer <= 0 )
                {
                    inUse = false;
                }
            }
        }
};

}

#endif
