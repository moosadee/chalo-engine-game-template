#include "Level.h"

#include "../../chalo-engine/Managers/TextureManager.hpp"

#include <fstream>

namespace RawrRinth
{

void Level::LoadMap( std::string filename )
{
    std::ifstream infile;
    infile.open( filename.c_str() );

    enemyCount = 20;
    itemCount = 30; //was 13

    if ( infile.bad() )
    {
        std::cerr<<"Error loading game map "<<filename<<std::endl;
    }
    std::string temp;
    infile>>temp;       //x
    infile>>temp;       //160
    infile>>temp;       //y
    infile>>temp;       //60
    infile>>temp;       //bgimage
    infile>>temp;       //background, none in this game.

    //BOTTOM LAYER
    for ( int j=0; j<TILEY; j++ )
    {
        for ( int i=0; i<TILEX; i++ )
        {
            infile>>temp;
            //Basic filmstrip coordinates, world coordinates
            int frameX = atoi( temp.c_str() ) * TILEWH;
            int frameY = 0;

            // RasPi workaround
            tile[0][i][j].fx = frameX;
            tile[0][i][j].fy = frameY;

            if ( frameX >= 3200 )
            {
                frameX = frameX - 3200;
                frameY = 32;
            }

            tile[0][i][j].x = i * TILEWH;
            tile[0][i][j].y = j * TILEWH;
            tile[0][i][j].region.X( 0 );
            tile[0][i][j].region.Y( 0 );
            tile[0][i][j].region.W( 32 );
            tile[0][i][j].region.H( 32 );
            tile[0][i][j].solid = false;

            tile[0][i][j].image.setPosition( sf::Vector2f( i * TILEWH, j * TILEWH ) );
            tile[0][i][j].image.setTexture( chalo::TextureManager::Get( "tileset2" ) );
            tile[0][i][j].image.setTextureRect( sf::IntRect( frameX, frameY, 32, 32 ) );

            //Collision? (rects, yay!)
            int t = tile[0][i][j].fx / TILEWH;
            if ( (t >= 1 && t <= 51) || (t >= 59 && t <= 80) || (t >= 87 && t <= 100) ||
                    (t >= 117 && t <= 127) || (t >= 135 && t <= 155) || (t >= 157 && t <= 159) || (t >= 187 && t <= 195) )
            {
                tile[0][i][j].solid = true;
            }
            else
            {
                tile[0][i][j].solid = false;
            }
        }
    }
    //MIDDLE LAYER
    for ( int j=0; j<TILEY; j++ )
    {
        for ( int i=0; i<TILEX; i++ )
        {
            infile>>temp;
            //Basic filmstrip coordinates, world coordinates
            int frameX = atoi( temp.c_str() ) * TILEWH;
            int frameY = 0;

            // RasPi workaround
            tile[1][i][j].fx = frameX;
            tile[1][i][j].fy = frameY;

            if ( frameX >= 3200 )
            {
                frameX = frameX - 3200;
                frameY = 32;
            }

            tile[1][i][j].x = i * TILEWH;
            tile[1][i][j].y = j * TILEWH;
            tile[1][i][j].region.X( 0 );
            tile[1][i][j].region.Y( 0 );
            tile[1][i][j].region.W( 32 );
            tile[1][i][j].region.H( 32 );
            tile[1][i][j].solid = true;

            tile[1][i][j].image.setPosition( sf::Vector2f( i * TILEWH, j * TILEWH ) );
            tile[1][i][j].image.setTexture( chalo::TextureManager::Get( "tileset2" ) );
            tile[1][i][j].image.setTextureRect( sf::IntRect( frameX, frameY, 32, 32 ) );

            //Collision?
            int t = tile[1][i][j].fx / TILEWH;
            if ( (t >= 1 && t <= 51) || (t >= 59 && t <= 80) || (t >= 87 && t <= 100) ||
                    (t >= 117 && t <= 127) || (t >= 135 && t <= 155) || (t >= 157 && t <= 159) || (t >= 187 && t <= 195) )
            {
                tile[1][i][j].solid = true;
            }
            else
            {
                tile[1][i][j].solid = false;
            }
        }
    }
    //TOP LAYER
    for ( int j=0; j<TILEY; j++ )
    {
        for ( int i=0; i<TILEX; i++ )
        {
            infile>>temp;
            //Basic filmstrip coordinates, world coordinates
            int frameX = atoi( temp.c_str() ) * TILEWH;
            int frameY = 0;

            // RasPi workaround
            tile[2][i][j].fx = frameX;
            tile[2][i][j].fy = frameY;

            if ( frameX >= 3200 )
            {
                frameX = frameX - 3200;
                frameY = 32;
            }

            tile[2][i][j].x = i * TILEWH;
            tile[2][i][j].y = j * TILEWH;

            tile[2][i][j].image.setPosition( sf::Vector2f( i * TILEWH, j * TILEWH ) );
            tile[2][i][j].image.setTexture( chalo::TextureManager::Get( "tileset2" ) );
            tile[2][i][j].image.setTextureRect( sf::IntRect( frameX, frameY, 32, 32 ) );


            //No collision for top layer
        }
    }
    infile.close();
}

void Level::DrawBottomLayer( sf::RenderWindow& window, float offsetX, float offsetY, int playerX, int playerY )
{
    int playerTileX = playerX / 32;
    int playerTileY = playerY / 32;
    int widthMargin = 31;
    int heightMargin = 15;

    int tx1, ty1, tx2, ty2;
    for ( int i=0; i<TILEX; i++ )
    {
        for ( int j=0; j<TILEY; j++ )
        {
            // Skip if off screen
            if ( i < playerTileX - widthMargin || i > playerTileX + widthMargin || j < playerTileY - heightMargin || j > playerTileY + heightMargin )
            {
                continue;
            }

            if ( tile[0][i][j].fx != 0 )
            {
                //Draw
                tile[0][i][j].image.setPosition( tile[0][i][j].x - offsetX, tile[0][i][j].y - offsetY );
                window.draw( tile[0][i][j].image );
//              masked_blit( tileset, buffer, tile[0][i][j].fx, tile[0][i][j].fy, tile[0][i][j].x - offsetX, tile[0][i][j].y - offsetY, tile[0][i][j].w, tile[0][i][j].h );

                //temp - draw bounding box
//              if ( tile[0][i][j].solid )
//              {
//                  tx1 = tile[0][i][j].x - offsetX;
//                  ty1 = tile[0][i][j].y - offsetY;
//                  tx2 = tile[0][i][j].x + tile[0][i][j].w - offsetX;
//                  ty2 = tile[0][i][j].y + tile[0][i][j].h - offsetY;
//                  rect( buffer, tx1, ty1, tx2, ty2, makecol( 255, 255, 0 ) );
//              }
            }
            if ( tile[1][i][j].fx != 0 )
            {
                tile[1][i][j].image.setPosition( tile[1][i][j].x - offsetX, tile[1][i][j].y - offsetY );
                window.draw( tile[1][i][j].image );
//              masked_blit( tileset, buffer, tile[1][i][j].fx, tile[1][i][j].fy, tile[1][i][j].x - offsetX, tile[1][i][j].y - offsetY, tile[1][i][j].w, tile[1][i][j].h );

                //temp - draw bounding box
//              if ( tile[1][i][j].solid )
//              {
//                  tx1 = tile[1][i][j].x - offsetX;
//                  ty1 = tile[1][i][j].y - offsetY;
//                  tx2 = tile[1][i][j].x + tile[1][i][j].w - offsetX;
//                  ty2 = tile[1][i][j].y + tile[1][i][j].h - offsetY;
//                  rect( buffer, tx1, ty1, tx2, ty2, makecol( 255, 0, 0 ) );
//              }
            }
        }
    }
}

void Level::DrawTopLayer( sf::RenderWindow& window, float offsetX, float offsetY, int playerX, int playerY )
{
    int playerTileX = playerX / 32;
    int playerTileY = playerY / 32;
    int widthMargin = 30;
    int heightMargin = 15;
    for ( int i=0; i<TILEX; i++ )
    {
        for ( int j=0; j<TILEY; j++ )
        {
            // Skip if off screen
            if ( i < playerTileX - widthMargin || i > playerTileX + widthMargin || j < playerTileY - heightMargin || j > playerTileY + heightMargin )
            {
                continue;
            }
            if ( tile[2][i][j].fx != 0 )
            {
                //Draw
                tile[2][i][j].image.setPosition( tile[2][i][j].x - offsetX, tile[2][i][j].y - offsetY );
                window.draw( tile[2][i][j].image );
//              masked_blit( tileset, buffer, tile[2][i][j].fx, tile[2][i][j].fy, tile[2][i][j].x - offsetX, tile[2][i][j].y - offsetY, tile[2][i][j].w, tile[2][i][j].h );
            }
        }
    }
}

}
