#ifndef _RAWR_ENEMY_HPP
#define _RAWR_ENEMY_HPP

#include "Rect.h"
#include "Enums.h"
#include "Character.h"
#include "TextEffect.h"

namespace RawrRinth
{

class Enemy
{
public:
    void Setup( int enemyIndex );
    void Update( Character players[2], int soundTimer, int soundTimer2, float gameTimer, int MaxText, TextEffect txteffect[] );
    void Move( Direction dir );
    void ChangeHP( float amount );

    void SetDeadTimer();
    float DeadTimer()
    {
        return deadTimer;
    }
    void Reset();

    bool Exists()
    {
        return exists;
    }
    void Exists( bool val )
    {
        exists = val;
    }
    void Draw( sf::RenderWindow& window, sf::Sprite image, int offsetX, int offsetY );

    float Exp()
    {
        return expAmt;
    }

    int X()
    {
        return position.x;
    }
    int Y()
    {
        return position.y;
    }
    int W()
    {
        return position.w;
    }
    int H()
    {
        return position.h;
    }

    int RX()
    {
        return collisionRegion.x;
    }
    int RY()
    {
        return collisionRegion.y;
    }
    int RW()
    {
        return collisionRegion.w;
    }
    int RH()
    {
        return collisionRegion.h;
    }

    float Speed()
    {
        return speed;
    }

    bool IsCollision( Character *player );

private:
    void IncrementFrame();

    float speed;
    float hp;
    int originalHP;
    int atk;
    bool exists;
    float expAmt;
    float frame;
    float deadTimer;
    int frameMax;
    float atkCounter;
    Rect position;
    Rect collisionRegion;
    Rect originalPosition;
    Action action;
    Direction direction;
    Type type;
};

}

#endif
