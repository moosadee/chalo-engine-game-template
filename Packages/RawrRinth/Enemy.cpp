#include "Enemy.h"

#include "../../chalo-engine/Utilities/Logger.hpp"

namespace RawrRinth
{

void Enemy::Setup( int enemyIndex )
{
    frameMax = 4;
    exists = true;
    atk = 1;
    atkCounter = -1;
    frame = IDLE;
    direction = RIGHT;
    action = WALKING;
    deadTimer = 0;

    collisionRegion.X( position.x + 5 );
    collisionRegion.Y( position.y + 5 );
    collisionRegion.W( 24 );
    collisionRegion.H( 24 );

    if ( enemyIndex % 4 == 0 )
    {
        type = EWOK;
        speed = 1.2f;
        position.w = 32;
        position.h = 48;
        hp = 20;
        expAmt = 6;
    }
    else if ( enemyIndex % 4 == 1 )
    {
        type = ROBOT;
        speed = 2.0f;
        position.w = position.h = 32;
        hp = 50;
        expAmt = 10;
    }
    else if ( enemyIndex % 4 == 2 )
    {
        type = KITTY;
        speed = 1.5f;
        position.w = position.h = 32;
        hp = 60;
        expAmt = 12;
    }
    else if ( enemyIndex % 4 == 3 )
    {
        type = SNAKE;
        speed = 1.0f;
        position.w = 48;
        position.h = 32;
        hp = 100;
        expAmt = 8;
    }

    int tile = 32;
    switch ( enemyIndex )
    {
        case 0:
            position.x = 29*tile;
            position.y = 6*tile;
        break;
        case 1:
            position.x = 36*tile;
            position.y = 21*tile;
        break;
        case 2:
            position.x = 60*tile;
            position.y = 34*tile;
        break;
        case 3:
            position.x = 72*tile;
            position.y = 42*tile;
        break;
        case 4:
            position.x = 15*tile;
            position.y = 17*tile;
        break;
        case 5:
            position.x = 7*tile;
            position.y = 19*tile;
        break;
        case 6:
            position.x = 18*tile;
            position.y = 29*tile;
        break;
        case 7:
            position.x = 3*tile;
            position.y = 56*tile;
        break;
        case 8:
            position.x = 28*tile;
            position.y = 54*tile;
        break;
        case 9:
            position.x = 77*tile;
            position.y = 3*tile;
        break;
        case 10:
            position.x = 62*tile;
            position.y = 5*tile;
        break;
        case 11:
            position.x = 73*tile;
            position.y = 13*tile;
        break;
        case 12:
            position.x = 78*tile;
            position.y = 26*tile;
        break;
        case 13:
            position.x = 73*tile;
            position.y = 54*tile;
        break;
        case 14:
            position.x = 33*tile;
            position.y = 49*tile;
        break;
        case 15:
            position.x = 22*tile;
            position.y = 41*tile;
        break;
        case 16:
            position.x = 24*tile;
            position.y = 28*tile;
        break;
        case 17:
            position.x = 47*tile;
            position.y = 29*tile;
        break;
        case 18:
            position.x = 42*tile;
            position.y = 26*tile;
        break;
        case 19:
            position.x = 57*tile;
            position.y = 41*tile;
        break;
    }

    originalHP = hp;
    originalPosition = position;
}

void Enemy::ChangeHP( float amount )
{
    hp += amount;
}

void Enemy::Update( Character players[4], int soundTimer, int soundTimer2, float gameTimer, int MaxText, TextEffect txteffect[] )
{
//    chalo::Logger::Out( "Enemy update", "Enemy::Update" );
//    chalo::Logger::OutValue( "soundTimer", soundTimer, "Enemy::Update" );
//    chalo::Logger::OutValue( "soundTimer2", soundTimer2, "Enemy::Update" );
//    chalo::Logger::OutValue( "gameTimer", gameTimer, "Enemy::Update" );

    if ( deadTimer > 0 )
    {
        deadTimer -= 0.1;

        if ( deadTimer <= 0 )
        {
            Reset();
        }
    }

    if ( exists )
    {
        collisionRegion.X( position.x + 5 );
        collisionRegion.Y( position.y + 5 );
        collisionRegion.W( 24 );
        collisionRegion.H( 24 );
        if ( atkCounter > 0 )
        {
            atkCounter -= 0.5;
        }
        else { atkCounter = -1; action = WALKING; }

        for ( int p = 0; p < 4; p++ )
        {
            if ( Exists() == true && players[p].Exists() && IsCollision( &players[p] ) )
            {
                if ( players[p].Is() == ATTACKING )
                {
                    chalo::Logger::Out( "Player is attacking", "Enemy::Update" );
//                    if ( soundTimer <= 0 )
//                    {
//                        play_sample( sndAttack, 255, 128, 1000, false );
                        soundTimer = 5.0;
                        ChangeHP( players[p].StrengthFloat() );

                        if ( hp <= 0 )
                        {
                            players[p].Exp( Exp() );
                        }

                        bool foundText = false;
                        //Setup fancy text crap
                        for ( int j=0; j<MaxText; j++ )
                        {
                            if ( foundText == false )
                            {
                                if ( txteffect[j].inUse == false )
                                {
                                    txteffect[j].Setup( players[p].StrengthString().c_str(), X()+3, Y(), 255, 255, 255 );
                                    foundText = true;
                                }
                            }
                        }
//                    }
                }//attacking

                //enemy attack
//                if ( soundTimer2 <= 0 && gameTimer > 20 )
//                if ( gameTimer > 20 )
//                {
                    chalo::Logger::Out( "Enemy is attacking", "Enemy::Update" );
//                    play_sample( sndDamage, 255, 128, 1000, false );
                    soundTimer2 = 10.0;
                    players[p].AddHP( -0.1 );
                    bool foundText = false;
                    //Setup fancy text crap
                    for ( int j=0; j<MaxText; j++ )
                    {
                        if ( foundText == false )
                        {
                            if ( txteffect[j].inUse == false )
                            {
                                txteffect[j].Setup( "-2", players[p].X()+3, players[p].Y(), 255, 255, 255 );
                                foundText = true;
                            }
                        }
                    }
//                }//not attacking
            }//if ( enemy[i].Exists() == true && IsCollision( &player, &enemy[i] ) )
        }

        if ( hp <= 0 )
        {
            exists = false;
            SetDeadTimer();
        }
    }
}

bool Enemy::IsCollision( Character *player )
{
    Rect pl, en;
    pl.x = player->RX();
    pl.y = player->RY();
    pl.w = player->RW();
    pl.h = player->RH();

    en.x = RX();
    en.y = RY();
    en.w = RW();
    en.h= RH();

    return (    pl.x            <       en.x+en.w &&
                pl.x+pl.w       >       en.x &&
                pl.y            <       en.y+en.h &&
                pl.y+pl.h       >       en.y );
}

void Enemy::Move( Direction dir )
{
    if ( exists )
    {
        if ( action != ATTACKING )
        {
            if ( dir == LEFT )
            {
                direction = dir;
                position.x = (int)(position.x - speed);
            }
            else if ( dir == RIGHT )
            {
                direction = dir;
                position.x = (int)(position.x + speed);
            }

            else if ( dir == UP )
            {
                position.y = (int)(position.y - speed);
            }
            else if ( dir == DOWN )
            {
                position.y = (int)(position.y + speed);
            }

            action = WALKING;
        }
        IncrementFrame();
    }
}

void Enemy::Reset()
{
    hp = originalHP;
    position = originalPosition;
    exists = true;
}

void Enemy::SetDeadTimer()
{
    deadTimer = 100;
}

void Enemy::Draw( sf::RenderWindow& window, sf::Sprite image, int offsetX, int offsetY )
{
    if ( exists )
    {
        if ( action != ATTACKING )
        {
            sf::IntRect textureRect( int(frame) * position.w, int(direction) * position.h, position.w, position.h );
            image.setPosition( position.x - offsetX, position.y - offsetY );
            image.setTextureRect( textureRect );
            window.draw( image );
//            masked_blit( source, destination,
//                (int)frame * position.w, (int)direction * position.h,
//                position.x - offsetX, position.y - offsetY, position.w, position.h );
        }
        else
        {
            int atk;
            if ( atkCounter > 5 ) { atk = 0; }
            else { atk = 1; }
            if ( direction == LEFT )
            {
                sf::IntRect textureRect( atk*position.w+(position.w*2), 2*position.h, position.w, position.h );
                image.setPosition( position.x - offsetX, position.y - offsetY );
                image.setTextureRect( textureRect );
                window.draw( image );
//                masked_blit( source, destination,
//                atk*position.w+(position.w*2), 2*position.h,
//                position.x - offsetX, position.y - offsetY, position.w, position.h );
            }
            else if ( direction == RIGHT )
            {
                sf::IntRect textureRect( atk*position.w, 2*position.h, position.w, position.h );
                image.setPosition( position.x - offsetX, position.y - offsetY );
                image.setTextureRect( textureRect );
                window.draw( image );
//                masked_blit( source, destination,
//                atk*position.w, 2*position.h,
//                position.x - offsetX, position.y - offsetY, position.w, position.h );
            }
        }
    }
}

void Enemy::IncrementFrame()
{
    frame += 0.15f;
    if ( frame >= frameMax )
        frame = 0.0f;
}

}
