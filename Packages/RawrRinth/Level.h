#ifndef _RAWR_LEVEL_HPP
#define _RAWR_LEVEL_HPP

#include "Tile.h"
#include <string>
#include <iostream>
#include <fstream>
#include "Rect.h"

#include <SFML/Graphics.hpp>

namespace RawrRinth
{

const int TILEX = 80;
const int TILEY = 60;
const int TILEWH = 32;      //tiles are square, w = h = 32

class Level
{
private:
    int enemyCount, itemCount;
//        int solidTiles[];
public:
    Tile tile[3][TILEX][TILEY];
    void LoadMap( std::string filename );
    void DrawTopLayer( sf::RenderWindow& window, float, float, int playerX, int playerY );
    void DrawBottomLayer( sf::RenderWindow& window, float, float, int playerX, int playerY );
    int EnemyCount()
    {
        return enemyCount;
    }
    int ItemCount()
    {
        return itemCount;
    }

    Rect CollisionRegion( int layer, int i, int j )
    {
        return tile[layer][i][j].region;
    }
};

}

#endif
