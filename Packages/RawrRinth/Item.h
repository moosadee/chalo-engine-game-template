#ifndef _RAWR_ITEM_HPP
#define _RAWR_ITEM_HPP

#include "Rect.h"
#include "BasicObject.h"
#include <iostream>
using namespace std;

namespace RawrRinth
{

enum ItemType { CHERRY = 0, ICECREAM = 1, EGG = 2, MONEY = 3 };

class Item : public BasicObject
{
private:
    //item behavior when picked up
    int addToHP;
    int addToScore;
    int addToMoney;
    string HPAdd;
    string MoneyAdd;
    ItemType type;
public:
    Item();
    int AdjustHP()
    {
        return addToHP;
    }
    int AdjustScore()
    {
        return addToScore;
    }
    int AdjustMoney()
    {
        return addToMoney;
    }
    string AdjustHPString()
    {
        return HPAdd;
    }
    string AdjustMoneyString()
    {
        return MoneyAdd;
    }
    string AdjustEggString()
    {
        return "Yay, egg!";
    }
    void Setup( int i );
    void Update();
    ItemType Type()
    {
        return type;
    }
};

}

#endif
