#ifndef _RAWR_GAME_HPP
#define _RAWR_GAME_HPP

#include <string>
#include <iostream>

#include <SFML/Graphics.hpp>

namespace RawrRinth
{

enum RawrGameState { sMENU = 0, sINGAME = 1, sGAMEOVER = 2 };

class Game
{
private:
    int screenWidth, screenHeight;
    bool done, fullscreen;
    RawrGameState state;
public:
    Game();
    ~Game();

    int ScreenWidth();
    int ScreenHeight();
    int FPS();

    bool Done()
    {
        return done;
    }
    void Done( bool val )
    {
        done = val;
    }

    void ToggleFullscreen();

    void BeginDraw();
    void EndDraw( sf::RenderWindow& window );

    RawrGameState State()
    {
        return state;
    }
    void State( RawrGameState val )
    {
        state = val;
    }
};

}

#endif
