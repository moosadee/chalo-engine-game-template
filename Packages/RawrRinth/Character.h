#ifndef _RAWR_CHARACTER_HPP
#define _RAWR_CHARACTER_HPP

#include "Rect.h"
#include "BasicObject.h"
#include "Enums.h"
#include <iostream>
using namespace std;

namespace RawrRinth
{

const float WALK_SPEED = 2.0;
const float RUN_SPEED = 3.0;

#define IDLE 1;

class Character : public BasicObject
{
protected:
    Direction direction;
    Action action;
    float speed;
    int score;
    float atkCounter;
    float fStrength;
    string szStrength;
    int exp, level, levelupExp;
    int powerup;
    int monies, eggs;
    float stamina;
    int playerNumber;
    int lastX, lastY;
    bool mousePlayer;

public:
    Character();
    void IncrementFrame();

    float WALK()
    {
        return WALK_SPEED;
    }

    float RUN()
    {
        return RUN_SPEED;
    }


    void SetDeadTimer();
    float DeadTimer()
    {
        return deadTimer;
    }
    void Reset( int otherX, int otherY );

    void Setup( int player );
    void Move( Direction dir );
    void Move( Direction dir, int otherX, int otherY );
    float Speed()
    {
        return speed;
    }
    void Speed( float val )
    {
        speed = val;
    }
    void Update( int otherX, int otherY );
    void Draw( sf::RenderWindow& window, sf::Sprite image, int offsetX, int offsetY );
    void AddScore( int amt )
    {
        score += amt;
    }
    Action Is()
    {
        return action;
    }
    int Score()
    {
        return score;
    }
    void BeginAttack();
    float StrengthFloat()
    {
        return fStrength;
    }
    string StrengthString()
    {
        return szStrength;
    }
    int Level()
    {
        return level;
    }
    int Powerup()
    {
        return powerup;
    }
    void Powerup( int val )
    {
        powerup = val;
    }
    int Monies()
    {
        return monies;
    }
    void AddMonies( int amt )
    {
        monies += amt;
    }
    int Eggs()
    {
        return eggs;
    }
    int Stamina()
    {
        return stamina;
    }
    void DrainStamina()
    {
        stamina -= 1;
        if ( stamina < 0 )
        {
            stamina = 0;
        }
    }
    void RestoreStamina()
    {
        stamina += 1.0;
        if ( stamina > 1000 )
        {
            stamina = 1000;
        }
    }
    void AddEggs()
    {
        eggs += 1;
    }
    bool Exp( int val )
    {
        exp += val;
        if ( exp >= levelupExp )
        {
            level++;
            levelupExp += level * 1.5;
            exp = 0;
            hp += 50;
            if ( hp > 100 )
            {
                hp = 100;
            }
            if ( level == 1 )
            {
                fStrength = -1.0;
                szStrength = "-10";
            }
            else if ( level == 2 )
            {
                fStrength = -1.5;
                szStrength = "-15";
            }
            else if ( level == 3 )
            {
                fStrength = -2.0;
                szStrength = "-20";
            }
            else if ( level == 4 )
            {
                fStrength = -2.5;
                szStrength = "-25";
            }
            else if ( level == 5 )
            {
                fStrength = -3.0;
                szStrength = "-30";
            }
            else if ( level == 6 )
            {
                fStrength = -3.5;
                szStrength = "-35";
            }
            return true;
        }
        return false;
    }
    int Exp()
    {
        return exp;
    }
    int ExpTilLevel()
    {
        return levelupExp;
    }
};

}

#endif
